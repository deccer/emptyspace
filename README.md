# emptyspace
Yet another space game.

## Controls
```ruby
 - W = accelerate forward
 - S = accelerate backward
 - A = accelerate to the left
 - D = accelerate to the right
 - Space = accelerate up
 - LCtrl = accelerate down
 - LShift = boost by factor 40
 - Q = roll left
 - E = roll right
 - R = stop acceleration
 - Mouse = define direction to accelerate
```

### Used Libraries
- GLFW
- GLAD
- PhysX
- nothings/stb (stb_image)
- glm
- assimp
